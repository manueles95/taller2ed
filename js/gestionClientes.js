var clientList;

function getCustomers(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function(){
    //4 means that the request has finiched loading all its content
    if(this.readyState == 4 && this.status == 200){
      //console.table(JSON.parse(request.responseText).value);
      clientList = request.responseText;
      procesarClientes();
    }
  }

  request.open("GET", url, true);
  request.send();
}// end getProducts


function procesarClientes(){

  var flagSource = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"
  var JSONclients = JSON.parse(clientList);
  var divTabla = document.getElementById("div-tabla");
  divTabla.innerText = "";
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");


  for (var i = 0; i < JSONclients.value.length; i++) {
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONclients.value[i].ContactName;
    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONclients.value[i].City;
    var columnaBandera = document.createElement("td");
    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");
    if (JSONclients.value[i].Country != "UK"){
      imgBandera.src = flagSource + JSONclients.value[i].Country + ".png"
    }
    else{
      imgBandera.src = flagSource + "United-Kingdom.png"
    }

    columnaBandera.appendChild(imgBandera);
    // columnaBandera.innerText = JSONclients.value[i].Country;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaBandera);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

}//end procesarProductos
