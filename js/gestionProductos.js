var productosObtenidos;

function getProducts(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function(){
    //4 means that the request has finiched loading all its content
    if(this.readyState==4 && this.status == 200){
      // console.table(JSON.parse(request.responseText).value);
      productosObtenidos = request.responseText;
      procesarProductos();
    }
  }

  request.open("GET", url, true);
  request.send();
}// end getProducts


function procesarProductos(){

  var JSONproductos = JSON.parse(productosObtenidos);
  var divTabla = document.getElementById("div-tabla")
  divTabla.innerText = "";
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");


  for (var i = 0; i < JSONproductos.value.length; i++) {
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONproductos.value[i].ProductName;
    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = JSONproductos.value[i].UnitPrice;
    var columnaStock = document.createElement("td");
    columnaStock.innerText = JSONproductos.value[i].UnitsInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

}//end procesarProductos
